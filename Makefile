# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/05 20:42:32 by ddombya           #+#    #+#              #
#    Updated: 2018/10/16 12:46:24 by ddombya          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

CC = gcc

CFLAGS = -Wall -Wextra -Werror

SRC = 	ft_atoi.c		\
		ft_bzero.c		\
		ft_error.c		\
		ft_is{alpha,ascii,digit,print,space,alnum}.c		\
		ft_mem{alloc,ccpy,chr,cmp,cpy,del,move,set}.c	\
		ft_put{char_fd,endl_fd,str_fd,nbr_fd}.c	\
		ft_put{char,endl,str,nbr}.c		\
		ft_str{cat,chr,clr,cmp,cpy,del,dup,equ,iter,iteri,join,str}.c	\
		ft_strlcat.c	\
		ft_strlen.c		\
		ft_str{map,mapi,rchr}.c		\
		ft_strn{cat,cmp,cpy,equ,str}.c		\
		ft_str{new,chr,split,str,sub,trim}.c	\
		ft_to{lower,upper}.c	\
		ft_lst{new,add,iter,delone,del,map}.c	\
		ft_itoa.c

OBJ = $(SRC:.c=.o)

$(NAME): libft.h
		@$(CC) $(CFLAGS) -c $(SRC)
		@ar rc $(NAME) $(OBJ)
		@ranlib $(NAME)

all: $(NAME)

clean:
		@rm -rf $(OBJ)

fclean: clean
		@rm -rf $(NAME)

re: fclean all
