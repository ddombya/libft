fftt/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 10:35:04 by ddombya           #+#    #+#             */
/*   Updated: 2018/10/16 12:46:28 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *nptr)
{
	int nb;
	int neg;

	nb = 0;
	neg = 0;
	while (ft_isspace(*nptr))
		nptr++;
	if (*nptr == '-')
		neg = *nptr++ > 0;
	else if (*nptr == '+')
		nptr++;
	while (ft_isdigit(*nptr))
	{
		nb = nb * 10 + (*nptr - '0');
		nptr++;
	}
	return (neg ? -nb : nb);
}
